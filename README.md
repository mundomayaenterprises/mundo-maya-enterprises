Running a for-profit, social enterprise that promotes Mayan families, artisans and coffee farmers in Guatemala by marketing their hand-made art, crafts and organic coffee.

Address: Spokane, WA, USA
|| Phone: 509-768-3193
